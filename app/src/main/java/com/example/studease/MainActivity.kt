package com.example.studease

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import androidx.databinding.DataBindingUtil
import com.example.studease.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.fragment_register.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

    }
    fun onRadioButtonClicked (view: View) {

        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                R.id.register_student ->
                    if (checked) {
                        register_student.setTextColor(Color.WHITE)
                        register_teacher.setTextColor(Color.RED)
                    }
                R.id.register_teacher ->
                    if (checked) {
                        register_student.setTextColor(Color.RED)
                        register_teacher.setTextColor(Color.WHITE)
                    }
            }
        }
    }
}
