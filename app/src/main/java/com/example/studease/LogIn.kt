package com.example.studease

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.studease.databinding.FragmentSampleBinding
import androidx.navigation.Navigation

class LogIn : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       val binding = DataBindingUtil.inflate<FragmentSampleBinding>(inflater, R.layout.fragment_sample, container, false)
        binding.btnRegister.setOnClickListener{ view: View ->
            Navigation.findNavController(view).navigate(R.id.action_logIn_to_register)
        }

        binding.btnLogin.setOnClickListener { view: View ->
            val usertype = "student"
            if (usertype == "student") {
                Navigation.findNavController(view).navigate(R.id.action_logIn_to_activityStudent)
            }
            else {
                Navigation.findNavController(view).navigate(R.id.action_logIn_to_activityTeacher)
            }
            }


        return binding.root
    }

}
